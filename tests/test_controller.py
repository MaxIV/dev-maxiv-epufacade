import pytest
from epufacade import epucontroller
from mock import MagicMock
from tango import DevFailed, AttrQuality, DevState


device_proxy_mock = MagicMock()
epucontroller.DeviceProxy = device_proxy_mock

# AttributeSubscriber tests


ATTR_NAME = "a/b/c/d"
DEFAULT_VALUE = 12.5
DEFAULT_QUALITY = AttrQuality.ATTR_VALID
DEFAULT_TIME = 123456789


@pytest.yield_fixture(params=["read_attribute", "event"])
def subscriber(request):
    subscribe = request.param == "event"
    # Mocking
    attribute_proxy_mock = MagicMock()
    attribute_proxy_mock.return_value = attribute_proxy_mock
    epucontroller.AttributeProxy = attribute_proxy_mock
    # No event, direc read
    if not subscribe:
        # Helpers
        def raise_devfailed(*args):
            raise DevFailed

        def read():
            read_mock = MagicMock()
            read_mock.value = DEFAULT_VALUE
            read_mock.quality = DEFAULT_QUALITY
            read_mock.time = DEFAULT_TIME
            return read_mock

        # Setup return values
        attribute_proxy_mock.subscribe_event.side_effect = raise_devfailed
        attribute_proxy_mock.read.side_effect = read
    # Allow event subscription
    else:
        # Mock callback
        def send_event(event_type, func):
            event_mock = MagicMock()
            event_mock.err = False
            event_mock.attr_value.value = DEFAULT_VALUE
            event_mock.attr_value.quality = DEFAULT_QUALITY
            event_mock.attr_value.time = DEFAULT_TIME
            func(event_mock)
            return 1

        attribute_proxy_mock.subscribe_event.side_effect = send_event
    # Create AttributeSubscriber
    subscriber = epucontroller.AttributeSubscriber(ATTR_NAME, True)
    # Assert AttributeProxy has been called
    attribute_proxy_mock.assert_called_with(ATTR_NAME)
    # Yield context
    yield subscriber


def test_value(subscriber):
    assert subscriber.attr == (DEFAULT_VALUE, DEFAULT_QUALITY, DEFAULT_TIME)


def test_attr(subscriber):
    assert subscriber.value == DEFAULT_VALUE


# EpuFacadeTest

ATTR_SUBSCRIBER_COUNT = 5
RATIOS = [1.0, 2, 2.4]

#
# def setup_controllers(ratio, undulator_mode):
#
#     return controller, subscriber_mock
#
#
@pytest.yield_fixture(params=RATIOS)
def epu_facade(request):
    # Mocking
    ratio = request.param
    # Setup controller and mocking
    # controller, subscriber_mock = setup_controllers(ratio, False)

    subscriber_mock = MagicMock()
    # Create one mock per AttributeSubscribers
    subscriber_mock.side_effect = lambda *x: MagicMock()
    device_proxy_mock = MagicMock()
    device_proxy_mock.return_value = device_proxy_mock
    epucontroller.AttributeSubscriber = subscriber_mock
    epucontroller.DeviceProxy = device_proxy_mock
    # Create object
    instance_name = "test"
    props = {
        "pseudo_motor": "pseudo/motor/name",
        "controller_alias": "EPU_TEST",
        "door_device": "door/device/name",
        "macro_name": "my_macro",
        "use_event": True,
        "undulator_mode_attr": "ctrl/phase/1/mode",
        "ratio": ratio,
        "gap_tolerance": 0.050,
    }
    controller = epucontroller.EpuFacade(instance_name, props)

    # AttributeSubscriber should have been called serveral times
    assert subscriber_mock.call_count == ATTR_SUBSCRIBER_COUNT
    # DeviceProxy should have been called once
    device_proxy_mock.assert_called_once_with("door/device/name")
    # Store AttibuteSubscriber mocks
    subscriber_mocks = {
        "position": controller._pseudo_position,
        "pseudo_state": controller._pseudo_state,
        "door_state": controller._door_state,
        "door_errors": controller._door_errors,
        "phase_mode": controller._undulator_mode,
    }
    yield (controller, subscriber_mocks, device_proxy_mock, ratio)


def test_read_value(epu_facade):
    # Unpack
    controller, sub_mocks, proxy_mocki, ratio = epu_facade
    # Set mock value
    sub_mocks["position"].value = 12
    read = controller.ReadOne(1)
    assert read == 12 / ratio


def test_start_one(epu_facade):
    # Unpack
    controller, sub_mocks, proxy_mock, ratio = epu_facade
    # Set mock value
    read_position = 0.0
    set_position = 12.001
    # Set mock value
    sub_mocks["position"].value = read_position
    controller.StartOne(1, set_position)
    # Assert command run macro has been called
    proxy_mock.command_inout.assert_called_once_with(
        "RunMacro", ["my_macro", str(float(set_position) * ratio), "EPU_TEST"]
    )


def test_stop_one(epu_facade):
    # Unpack
    controller, sub_mocks, proxy_mock, _ = epu_facade
    # Set door state
    sub_mocks["door_state"].value = DevState.ON
    controller.StopOne(1)
    # Nothing happened
    proxy_mock.command_inout.assert_not_called()
    # Set door state
    sub_mocks["door_state"].value = DevState.RUNNING
    controller.StopOne(1)
    # Macro server should have been called
    proxy_mock.command_inout.assert_called_once_with("StopMacro")


def test_state_one(epu_facade):
    # Unpack
    controller, sub_mocks, proxy_mock, _ = epu_facade
    # Set states
    sub_mocks["door_state"].value = DevState.RUNNING
    sub_mocks["pseudo_state"].value = DevState.ON
    # Set lock
    proxy_mock.is_locked.return_value = False
    state, status = controller.StateOne(1)
    # Assert results
    assert state == DevState.MOVING
    assert "{}".format(DevState.ON) in status
    assert "{}".format(DevState.RUNNING) in status
    assert "locked" not in status
    # Set alarm state
    sub_mocks["door_state"].value = DevState.ALARM
    sub_mocks["door_errors"].value = "some_errors"
    state, status = controller.StateOne(1)
    # Assert errors
    assert state == DevState.ALARM
    assert "{}".format(DevState.ON) in status
    assert "{}".format(DevState.ALARM) in status
    assert "some_errors" in status


def test_locked(epu_facade):
    # Unpack
    controller, sub_mocks, proxy_mock, _ = epu_facade
    # Set states
    sub_mocks["door_state"].value = DevState.RUNNING
    sub_mocks["pseudo_state"].value = DevState.ON
    # Set lock
    proxy_mock.is_locked.return_value = True
    state, status = controller.StateOne(1)
    assert state == DevState.DISABLE
    assert "locked" in status


def test_gap_tolerance(epu_facade):
    # Unpack
    controller, sub_mocks, proxy_mock, ratio = epu_facade
    # Set mock value
    position = 12.0
    # Read position has to be scalled with ratio
    read_position = position * ratio
    # Set position lower than the ratio
    set_position = position + (controller.gap_tolerance / 2.0)
    # Set mock value
    sub_mocks["position"].value = read_position
    controller.StartOne(1, set_position)
    # Assert that nothing has moved
    proxy_mock.command_inout.assert_not_called()
    # Now move further than the tolerance
    set_position = position + (controller.gap_tolerance * 2.0)
    controller.StartOne(1, set_position)
    assert proxy_mock.command_inout.call_count > 0


def test_phase_mode(epu_facade):
    # Unpack
    controller, sub_mocks, proxy_mock, ratio = epu_facade
    # Helical Modes:
    for i in range(1, 7):
        sub_mocks["phase_mode"].value = i
        mode = controller.getUndulatorMode()
        assert mode.startswith(str(i))
        if i < 4:
            assert "Helical" in mode
        else:
            assert "Inclined" in mode
