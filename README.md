Sardana movable (Motor controller) to control an [ID EPU](https://gitlab.maxiv.lu.se/kits-maxiv/lib-maxiv-epu_ctrl) from an extrernal Sardana Pool.

One controller move one axis (like Gap or Phase ...)

The Position value is directly read from a PseudoMotor.

Write position is bind to a Macro of the EPU MacroServer.

The Stop command is bind to the StopMacro command of the EPU MacroServer.

The State is computed from the EPU Door and a EPU PseudoMotor:
 - "Moving" if the door is in "running" state
 - "Moving" if the PseudoMotor is in moving state
It means that the State will be moving until the correction macro is done.

Test
===

Run test using PyTest

```bash
$ py.test -s
```

or

```bash
$ python -m pytest -s
```


