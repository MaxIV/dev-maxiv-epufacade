from sardana.pool.controller import (
    MotorController,
    Type,
    Description,
    DefaultValue,
    DataAccess,
    FGet,
    Access,
)
from tango import DevFailed, DeviceProxy, AttributeProxy, EventType, DevState


class AttributeSubscriber(object):
    def __init__(self, attribute_name, use_event):
        self.attribute_name = attribute_name
        self.attribute_proxy = AttributeProxy(attribute_name)
        self._attr_value = None
        self._attr_quality = None
        self._attr_timestamp = None
        self.event_id = None
        if not use_event:
            return
        try:
            self.event_id = self.attribute_proxy.subscribe_event(
                EventType.CHANGE_EVENT, self.on_event
            )
        except DevFailed:
            self.event_id = None

    @property
    def attr(self):
        if self.event_id:
            return self._attr_value, self._attr_quality, self._attr_timestamp
        else:
            value = self.attribute_proxy.read()
            return value.value, value.quality, value.time

    @property
    def value(self):
        return self.attr[0]

    def on_event(self, event):
        if not event.err:
            self._attr_value = event.attr_value.value
            self._attr_quality = event.attr_value.quality
            self._attr_timestamp = event.attr_value.time


class EpuFacade(MotorController):
    ctrl_properties = {
        "pseudo_motor": {
            Type: str,
            DefaultValue: "",
            Description: "Pseudo motor tango device name (where to read)",
        },
        # the undulator mode gets it's own property, since the mode attribute belongs to the
        # controller and the phase to the motor
        "undulator_mode_attr": {
            Type: str,
            DefaultValue: "",
            Description: "Specifies the Sardana controller attribute which reads the EPU mode",
        },
        "controller_alias": {
            Type: str,
            DefaultValue: "",
            Description: "Controller alias, used as macro argument",
        },
        "door_device": {
            Type: str,
            DefaultValue: "",
            Description: "Door tango device",
        },
        "macro_name": {
            Type: str,
            DefaultValue: "",
            Description: "Macro name, used in StartOne",
        },
        "ratio": {
            Type: float,
            DefaultValue: 1.0,
            Description: "Ratio to convert Gap value to mm",
        },
        "use_event": {
            Type: bool,
            DefaultValue: False,
            Description: "Allow event subscription",
        },
        "gap_tolerance": {
            Type: float,
            DefaultValue: 0.050,
            Description: "Gap min step size. The AndThi gap safety protection limit",
        },
        "extra_args": {
            Type: (str,),
            DefaultValue: [],
            Description: "Extra static macro arguments",
        },
    }

    # here, the undulator mode is added as a controller attribute
    ctrl_attributes = {
        "Undulator_Mode": {
            Type: str,
            Description: "Mode of the undulator polarization",
            FGet: "getUndulatorMode",
            Access: DataAccess.ReadOnly,
        }
    }

    # undulator mode ought to be the same for all EPU Facade class, hard-coded here
    def getUndulatorMode(self):
        mode_value = self._undulator_mode.value
        if mode_value in (1, 2, 3):
            return str(mode_value) + " - Helical"
        if mode_value in (4, 5, 6):
            return str(mode_value) + " - Inclined"
        else:
            return "Error, undefined: " + str(self._undulator_mode)

    def __init__(self, inst, props):
        MotorController.__init__(self, inst, props)
        pseudo_position_attr = "/".join([self.pseudo_motor, "Position"])
        pseudo_state_attr = "/".join([self.pseudo_motor, "State"])
        door_state_attr = "/".join([self.door_device, "State"])
        evt = self.use_event
        self._pseudo_position = AttributeSubscriber(pseudo_position_attr, evt)
        self._pseudo_state = AttributeSubscriber(pseudo_state_attr, evt)
        self._door = DeviceProxy(self.door_device)
        if self.undulator_mode_attr:
            self._undulator_mode = AttributeSubscriber(
                self.undulator_mode_attr, evt
            )
        self._door_state = AttributeSubscriber(door_state_attr, evt)
        self._door_errors = AttributeSubscriber(
            "/".join([self.door_device, "Error"]), evt
        )

    def ReadOne(self, axis):
        return self._pseudo_position.value / float(self.ratio)

    def StateOne(self, axis):
        door_state = self._door_state.value
        pseudo_state = self._pseudo_state.value
        door_errors = self._door_errors.value
        locked = False
        try:
            not_allowed = "The device is locked by the controlroom"
            allowed = "It is allowed to move the device"
            locked = self._door.is_locked()
            lock_msg = not_allowed if locked else allowed
        except DevFailed:
            locked = True
            lock_msg = "Impossible to read the lock"
        status = "{}\nDoor state: {}\nPseudo Motor state: {}\n"
        status = status.format(lock_msg, door_state, pseudo_state)
        if locked:
            return DevState.DISABLE, status
        elif door_state == DevState.ALARM:
            return door_state, status + "Door errors: {}".format(door_errors)
        elif pseudo_state == DevState.ALARM:
            return pseudo_state, status
        else:
            state = (
                pseudo_state
                if door_state != DevState.RUNNING
                else DevState.MOVING
            )
            return state, status

    def StartOne(self, axis, position):
        if abs(position - self.ReadOne(None)) >= self.gap_tolerance:
            self._log.info("Moving to {}".format(position))
            self._door.command_inout(
                "RunMacro",
                [
                    self.macro_name,
                    str(position * float(self.ratio)),
                    self.controller_alias,
                ],
            )
        else:
            err = (
                "Going from {} to {} is under the minimum id gap step size {}"
            )
            self._log.error(
                err.format(position, self.ReadOne(None), self.gap_tolerance)
            )

    def StopOne(self, axis):
        if self._door_state.value == DevState.RUNNING:
            self._door.command_inout("StopMacro")

    def AbortOne(self, axis):
        self.StopOne(axis)
