#!/usr/bin/env python

from setuptools import setup

setup(name="python-sardana-epufacade",
      version="0.1.6",
      packages=["epufacade"],
      license="GPLv3",
      description="A Sardana movable interface for insertion device EPU",
      author="Antoine Dupre",
      author_email="antoine.dupre@maxiv.lu.se",
      url="http://www.maxlab.lu.se")
